// задача №1

const arraylines = ["travel", "hello", "eat", "ski", "lift"];
const count = arraylines.filter((str) => str.length > 3).length;
console.log(count);

// задача №2

const people = [
  { name: "Іван", age: 25, sex: "чоловіча" },
  { name: "Олександра", age: 30, sex: "жіноча" },
  { name: "Володимер", age: 22, sex: "чоловіча" },
  { name: "Катерина", age: 28, sex: "жіноча" },
];

const filteredPeople = people.filter((person) => person.sex === "чоловіча");
console.log(filteredPeople);

// задача №3

function filterBy(arr, dataType) {
  return arr.filter((elem) => {
    if (typeof elem === dataType || elem === null) {
      return true;
    }
    return false;
  });
}

const data = ["hello", "world", 23, "23", null];
const filteredData = filterBy(data, "number");
console.log(filteredData);
